package com.example.polegeaCats.ui.screens

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.polegeaCats.data.apiservice.CatsApi
import com.example.polegeaCats.ui.model.CatsUIModel
import com.example.polegeaCats.ui.model.mapper.CatsDtoUIModelMapper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class DetailViewModel(id:String): ViewModel() {
    private val _uiState = MutableStateFlow<CatsUIModel>(CatsUIModel("","","","","","",""))
    val uiState: StateFlow<CatsUIModel> = _uiState.asStateFlow()

    private var mapper: CatsDtoUIModelMapper = CatsDtoUIModelMapper()
    init {
        viewModelScope.launch {
            val breeds = CatsApi.retrofitService.getCat(id)
            val photoListDto = CatsApi.retrofitService.getCatImageRx(id)[0]
            _uiState.value = mapper.map2(breeds, photoListDto)
        }
    }
}